/*
    A simple js mp3 player
*/
window.player = function() {
    this.regex_track_parser = /.*\/(.*.mp3)/
    this.audio = document.getElementById("currentTrack")
    this.drip = document.getElementById("drip")
    this.trackTitle = document.getElementById('trackTitle')

    //
    // parse the track list response and build playlist
    //
    this.parseTracks = async function(data) {
        var playlist = document.getElementById("playlist")
        var subdirs = document.getElementById("subdirs")

        function getListItem(path, title, index, className) {
            let template = document.createElement('template');
            template.innerHTML = `
                <li class="CLASS_NAME" data-index="INDEX">
                    <div class="title">TITLE</div>
                </li>`
                .replace('CLASS_NAME', className)
                .replace('INDEX', index)
                .replace('TITLE', title)
            return template.content.firstElementChild
        }
        
        // load sub-directories
        subdirs.innerHTML = ''
        for (var i = 0; i < data.subdirs.length; i++) {
            title = data.subdirs[i] === '..' ? '[Back]' : data.subdirs[i].replace('media/', '').replaceAll('/', ' ')
            var item = getListItem(data.subdirs[i], title, i, 'subdir')
            item.onclick = function(path) {
                return function(event) {
                    window.location = path
                }
            }(data.subdirs[i])
            subdirs.appendChild(item)
        }

        // load track-list
        playlist.innerHTML = ''
        for (var i = 0; i < data.tracks.length; i++) {
            let match = data.tracks[i].match(this.regex_track_parser)
            let title = match ? match[1].replaceAll('_', ' ').trim() : data.tracks[i]
            var item = getListItem(data.tracks[i], title, i, 'track')
            item.onclick = function(index, path) {
                return function(event) {
                    player.store('currentTrack', index)
                    player.store('currentTrackPath', path)
                    player.applySettings(true)
                }
            }(i, data.tracks[i])
            playlist.appendChild(item)
        }
    }

    //
    // proxy localStorage to include window location
    //
    this.store = function(name, value) {
        let key = [name, window.location.pathname].join('|')
        if (value === undefined) {
            return localStorage.getItem(key)
        } else {
            return localStorage.setItem(key, value)
        }
    }

    //
    // pull settings from local storage and update UI
    //
    this.applySettings = function(play) {
        function setActive(condition, element) {
            if (condition) {
                element.classList.add('active')
                element.scrollIntoViewIfNeeded()
            } else {
                element.classList.remove('active')
            }
            
        }

        this.audio.focus()
        if (player.store('search')) {
            document.getElementById('search').value = player.store('search')
            document.getElementById('search').onkeyup()
        }
        const items = document.querySelectorAll('.track');
        let track = this.store('currentTrack')
        for (let i = 0; i < items.length; i++)
            setActive((''+i) == track, items[i])
        setActive('true' == this.store('ctrl_shuffle'), document.getElementById('ctrl_shuffle'))
        setActive('true' == this.store('ctrl_repeat'), document.getElementById('ctrl_repeat'))
        this.audio.loop = 'true' == this.store('repeat')
        var path = this.store('currentTrackPath')
        if (path && (play || decodeURI(this.audio.src).indexOf(this.store('currentTrackPath')) < 0)) {
            this.audio.src = this.store('currentTrackPath')
            this.trackTitle.innerText = this.store('currentTrackPath')
            this.audio.currentTime = this.store('currentTime:') || 0
            this.audio.load();
            this.audio.play().catch(error => {
                if (error && error.name === "NotAllowedError")
                    console.warn("Thanks, Google, for breaking autoplay")
                else // otherwise go to next track
                    document.getElementById('ctrl_next').click()
            })
        }
    }

    //
    // add control bindings
    //
    this.addControlBindings = function() {
        function makeToggleable(name, exclusiveWithName) {
            document.getElementById(name).onclick = function(event) {
                player.store(name, 'false' == player.store(name))
                if ('true' == player.store(name))
                    player.store(exclusiveWithName, false)
                player.applySettings()
            }
        }
        function makeClickable(name, action) {
            document.getElementById(name).onclick = function(event) {
                event.target.classList.add('active')
                setTimeout(_ => event.target.classList.remove('active'), 100)
                action()
            }
        }

        // keep track of current playlist time
        setInterval(this.audio.onseeked, 1000)

        // adjust playlist height and audio controls
        let playlist = document.getElementsByClassName('playlist')[0]
        function lolCSS() {
            var newHeight = Math.min(playlist.firstElementChild.clientHeight, Math.floor(window.innerHeight - playlist.offsetTop - 33))
            console.log(playlist.scrollHeight, window.innerHeight, playlist.offsetTop, newHeight)
            playlist.style.height = newHeight + 'px'
            var newWidth = document.getElementsByClassName('subdir')[0].clientWidth
            player.audio.style.width = newWidth + 'px'
            player.drip.style.width = newWidth - 93 + 'px'
        }
        lolCSS()
        window.addEventListener('resize', lolCSS, true);
        
        // buttons
        makeToggleable('ctrl_shuffle', 'ctrl_repeat')
        makeToggleable('ctrl_repeat', 'ctrl_shuffle')
        makeClickable('ctrl_rewind', _ => this.audio.currentTime -= 10)
        makeClickable('ctrl_forward', _ => this.audio.currentTime += 10)
        makeClickable('ctrl_next', _ => this.audio.onended('next'))
        makeClickable('ctrl_prev', _ => this.audio.onended('prev'))

        // key press
        document.onkeydown = function(event) {
            this.audio = document.getElementById("currentTrack")
            if (document.activeElement.id === 'search')
                return
            else if (event.code == 'ArrowRight' || event.code == 'KeyD')
                this.audio.currentTime += 10
            else if (event.code == 'ArrowLeft' || event.code == 'KeyA')
                this.audio.currentTime -= 10
            else if (event.code == 'Space' && document.activeElement != this.audio)
                this.audio.duration > 0 && !this.audio.paused ? this.audio.pause() : player.applySettings(true)
        }

        // next-track behavior
        this.audio.onended = function(event) {
            player.store('currentTime:', 0)
            if ('true' == player.store('ctrl_repeat')) { // repeat
                player.audio.currentTime = 0
                player.applySettings(true)
            } else if (typeof(event) !== 'string' && 'true' == player.store('ctrl_shuffle')) { // shuffle
                var playlist = document.getElementById("playlist")
                playlist.children[Math.floor(Math.random() * playlist.children.length)].click()
            } else if (event === 'prev') { // prev with wrap-around
                (document.querySelector('#playlist .active').previousElementSibling ||
                 document.querySelector('#playlist').lastElementChild).click()
            } else { // next with wrap-around
                (document.querySelector('#playlist .active').nextElementSibling ||
                 document.querySelector('#playlist').firstElementChild).click()
            }
        }

        // search bar
        document.getElementById('search').onkeyup = function(event) {
            let query = document.getElementById('search').value.toLowerCase()
            player.store('search', query.trim())
            var regexQuery = RegExp('$^')
            try { regexQuery = RegExp(query) } catch (error) {}
            var allItems = document.querySelectorAll('ul > li')
            allItems.forEach(function(item, index) {
                let txt = item.innerText.trim().toLowerCase()
                if (regexQuery.test(txt) || txt.indexOf(query) >= 0 || txt === '[back]') {
                    item.style.display = 'block'
                } else {
                    item.style.display = 'none'
                }
            })
        }
    }

    //
    // Every player needs some drip
    //
    this.audio.onseeked = function() {
        player.store('currentTime:', player.audio.currentTime)
        let context = player.drip.getContext('2d')
        var inc = player.drip.width / player.audio.duration
        context.fillStyle = 'black'
        context.fillRect(0, 0, player.drip.width, player.drip.height)
        context.fillStyle = 'red'
        context.strokeStyle = 'black'
        for (i = 0; i < player.audio.buffered.length; i++) {
            var startX = player.audio.buffered.start(i) * inc
            var endX = player.audio.buffered.end(i) * inc
            context.fillRect(startX, 0, endX, player.drip.height)
            context.rect(startX, 0, endX, player.drip.height)
            context.stroke()
        }
    }

    //
    // Save track position before refresh
    //
    window.onbeforeunload = function(event) {
        player.store('currentTime:', player.audio.currentTime)
    }

    //
    // fetch the track list for current directory
    //
    fetch('/api/list?path=' + encodeURIComponent(location.pathname))
        .then(response => response.json().then(tracks => {
            this.parseTracks(tracks)
            this.addControlBindings()
            this.applySettings()
        }))

    return this
}()

// Google broke auto-play https://goo.gl/xX8pDD
seenFirstInteraction = false
interactions = ['click', 'touchstart']
interactions.forEach(interaction => {
    listener = _ => {
        if (!seenFirstInteraction) {
            seenFirstInteraction = true
            interactions.forEach(interaction => document.body.removeEventListener(interaction, listener))
            document.getElementById("currentTrack").play().catch(_ => '')
        }
    }
    document.body.addEventListener(interaction, listener)
})
