import os
import glob
import signal
import asyncio

from baize.asgi import FileResponse
from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
from fastapi.responses import RedirectResponse, HTMLResponse
from starlette.requests import Request

app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")

@app.get("/")
async def index():
    return RedirectResponse(url='/media/')

@app.get("/favicon.ico")
async def favicon():
    return RedirectResponse(url='/static/assets/favicon.ico')

@app.get("/api/list")
async def api_list(path: str):
    if '..' in path or not path.startswith('/media') :
        raise Exception(path + ' is not allowed')

    tracks = []
    subdirs = [] if path == '/media/' else ['..']
    for item in glob.glob('.' + os.path.join(path, '*')):
        if os.path.isdir(item):
            subdirs.append(item.replace("./", '/'))
        else:
            tracks.append(item.replace("./", '/'))

    return {
        'tracks': sorted(tracks),
        'subdirs': sorted(subdirs)
    }

@app.middleware("http")
async def media(request: Request, call_next):
    if not request.url.path.startswith('/media'):
        return await call_next(request)
    if 'Range' not in request.headers:
        with open('static/index.html', mode='r') as file:
            return HTMLResponse(file.read())
    if '..' in request.url.path:
        raise Exception(request.url.path + ' is not allowed')

    full_path = './' + request.url.path
    start, end = request.headers.get('Range').strip().strip('bytes=').split('-')
    start = int(start)
    size = os.stat(full_path)[6]
    end = min(size-1, start+10)
    return FileResponse(full_path, headers={
        'Accept-Ranges': 'bytes',
        'Content-Range': 'bytes %s-%s/%s' % (start, end, size),
        'Content-Length': str(size-start)
    })

if __name__ == "__main__":
    from uvicorn import Server, Config

    # create shutdown hook for SIGHUP
    passed_by_ref_obj = {'should_restart': True}
    def _signal_handler(*args):
        # override default behavior when SIGHUP is received
        passed_by_ref_obj['should_restart'] = True
        # uvicorn doesn't have a asyncio.Event() based trigger, so send SIGINT to stop it
        os.kill(os.getpid(), signal.SIGINT)

    # start server - restarting periodically via SIGHUP from cron
    while passed_by_ref_obj['should_restart']:
        # default behavior is to shutdown if SIGINT or SIGTERM are received
        passed_by_ref_obj['should_restart'] = False
        loop = asyncio.get_event_loop()
        loop.add_signal_handler(signal.SIGHUP, _signal_handler)
        server = Server(config=(Config(app=app, loop=loop, host="0.0.0.0", port=1554)))
        loop.run_until_complete(server.serve())
