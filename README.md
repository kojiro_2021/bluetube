# Bluetube

Keep track of mp3s and serve via html player with docker.

## Setup

Build the docker container and populate the media directory.
```bash
bin/build
bin/download \
    https://www. youtube.com/watch?v=_p8rcAPPsvU \
    https://www. youtube.com/watch?v=Ug0y1ZhdHT0
bin/download_playlist \
    passion_of_covers \
    https://www. youtube.com/playlist?list=OLAK5uy_k7zdPY2zGoT_8D5V0xJfbQ8GwRfzie27U
```

Add existing any audio content (or symlink) to the media directory and it will be available organized as it is on disk.

### Serve files from docker
```bash
bin/serve --detach # or `bin/serve` to stay attached and see server logs
```

### Serve file locally
```bash
brew install python3
pip3 install fastapi uvicorn aiofiles asyncio baize
python3 server.py
```

# Why?

Partly because iTunes is unusable, partly because Google treats users like cattle and the ads are insipid at best, partly because streaming services remove content randomly or don't have it at all, and partly because all the other light-weight mp3 mplayers probably come with a botnet.

Now you can host a personal mp3 streamer locally, on a VPS, or anywhere with docker container hosting
