FROM ubuntu:bionic

RUN apt-get update \
    && apt-get remove python \
    && apt-get autoremove \
    && apt-get install -y \
        python3.8 \
        python3-pip \
        # it's a python server... so restart it periodically with cron
        cron \
        # 😱 use docker to help protect against 💀ffmpeg💀 invocations
        ffmpeg \
    && rm -rf \
        /var/lib/apt/lists/* \
        /var/tmp/*

# TODO: switch back to youtube-dl once a new primary repo emerges
RUN python3.8 -m pip install fastapi uvicorn aiofiles asyncio baize yt-dlp

# since we are already using docker, host the server in it too
COPY server.py server.py
COPY static static

# memory leaks from starlette's FileResponse, so restart periodically
RUN echo "0 * * * * pkill --signal SIGHUP --full server.py" >> /etc/crontab

ENTRYPOINT [ "/bin/bash", "-l", "-c" ]
